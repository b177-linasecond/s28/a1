fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => json.map(function(json) {return json.title}))
.then(title => console.log(title))

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) =>  response.json())
.then(json => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => {return response.json()})
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`))

fetch('https://jsonplaceholder.typicode.com/todos', {
	method : 'POST',
	headers : {
		'Content-Type' : 'application/json'
	},
	body : JSON.stringify({
		id : 201,
		title : 'Created To Do List Item',
		completed : 'false',
		userId : 1
	}),
})
.then((response) => {return response.json()})
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method : 'PUT',
	headers : {
		'Content-Type' : 'application/json'
	},
	body : JSON.stringify({
		dateCompleted : "Pending",
		description : 'To update my to do list with a different data strcuture',
		id : 1,
		status : 'Pending',
		title : 'Updated To Do List Item',
		userId : 1
	}),
})
.then((response) => {return response.json()})
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method : 'PATCH',
	headers : {
		'Content-Type' : 'application/json'
	},
	body : JSON.stringify({
		dateCompleted : '07/09/21',
		status : 'Completed'
	}),
})
.then((response) => {return response.json()})
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method : 'DELETE'
})